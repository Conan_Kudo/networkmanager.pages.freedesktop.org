+++
date = "2021-06-16T12:00:00+01:00"
title = "NetworkManager 1.32"
tags = ["release"]
draft = false
author = "Thomas Haller"
description = "What's new"
weight = 10
+++

Four months after [1.30 release](../networkmanager-1-30), please welcome
1.32.0, the new major release of NetworkManager. Here are some of the
changes that it brings.

## Firewall Mode and nftables

When activating a "shared" profile, NetworkManager will enable NAT for IPv4.
Until now, it could only call `iptables` to configure masquerading. With this
new version, it can also configure nftables for the same purpose. The firewall backend
can be configured via the new `[main].firewall-backend` option in
[NetworkManager.conf](https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/NetworkManager.conf.html).
In absence of an explicit configuration the default is "nftables" unless
`/usr/sbin/nft` does not exist and `/usr/sbin/iptables` exists.

Note that on current Fedora the SELinux policy prevents NetworkManager from
communicating with `nft`, so beware of that potential problem.

Firewalld backend is planned to be added in the future.

## Ethtool Pause Setting (Ethernet Flow Control)

NetworkManager gained support for three new properties: "ethtool.pause-autoneg",
"ethtool.pause-rx" and "ethtool.pause-tx". These configure Ethernet Flow Control,
also known as Pause Frames ([[Red Hat Guide]](https://access.redhat.com/articles/1391433))
and correspond to
```
ethtool -A|--pause devname [autoneg on|off] [rx on|off] [tx on|off]
```
from the [ethtool tool](https://www.man7.org/linux/man-pages/man8/ethtool.8.html).

## Promiscuous Mode

A new property "ethernet.accept-all-mac-addresses" can be configured to
set the promisc mode of the interface. It configures whether the NIC
filters ethernet frames destined for other MAC addresses.

## Reverse DNS Lookup for hostname

NetworkManager can do a reverse lookup for configured IP addresses to configure
the hostname. This can be controlled by the `hostname` settings of the
profile. Note that most systems have a default hostname already configured,
so when we lookup the hostname for the IP address, we must only consult the DNS
servers. Previously NetworkManager used libc's `getnameinfo()`, which depending
on the NSS configuration, would return the already configured hostname without
asking DNS.

Now NetworkManager will call `getnameinfo()` with NSS configured to only use
DNS. Alternatively, if systemd-resolved is enable, NetworkManager tries to
resolve the address using the D-Bus API in a way that forces use of DNS.

## API Changes

NetworkManager makes an effort to not break API or existing users/applications
that rely on a certain behavior. In practice, any change, new feature or bugfix has
the potential to badly affect a user who did not expect this (xkcd#1172). So NetworkManager's
main commitment is to not break existing workflows, but we are OK with changing API
as long as nobody cares.

This release brings such API changes, that hopefully won't bother users.

- NetworkManager's D-Bus API had home grown PropertiesChanged signals, that predate
the standard D-Bus properties. Those signals were deprecated since version 1.6.0 (2017)
and  emitted together with the "org.freedesktop.DBus.Properties.PropertiesChanged" signals.
These signals are now dropped. There are no known users of this D-Bus API.

- Hide struct definitions for `NMSimpleConnection`, `NMSetting` and `NMSetting` subclasses
from the public headers of libnm. These classes were never intended to be subclassed
by the user, and thus these typedefs should only be used as opaque types.
Having the structures in the headers limits what we can do with them. They are now
hidden from the headers. For now, the struct sizes are still unchanged and there is no
ABI change. Thus existing application that were compiled against a previous version will continue
to work with libnm 1.32.0. This API/ABI was always supposed to be internal, and in the future
we might also change the ABI. So if you are affected by this change, we really want to hear from
you.

- The primary key of a connection profile is "connection.uuid". Previously, this property
also accepted some values which were not valid string representations for [rfc4122](https://tools.ietf.org/html/rfc4122)
UUIDs. Now, NetworkManager enforces that "connection.uuid" is always a valid, all lower case
UUID. In an attempt to not break existing setups, previously accepted but invalid UUIDs
are now normalized. This is a change visible to the user. If you have any profiles that refer
to other profiles by such an invalid UUID, via the "parent" or "master" property, then this
normalization breaks those profiles. In practice, nobody should use such invalid UUIDs and no
client tools are known to create them.

- Fix WireGuard API in vala bindings. The vala bindings are still marked as experimental
and WireGuard API was generated wrongly. This is now fixed, but is also an API break.

- If a profile doesn't specify any traffic control options via the `tc` settings,
  then NetworkManager will now no longer touch the TFilter/QDisc settings. Previously
  it would always wipe the existing tc configuration. You can restore the previous
  behavior by ensuring that a tc section is present with
  ```
  nmcli connection modify "$PROFILE" tc.tfilter ''
  ```
  or remove them with
  ```
  nmcli connection modify "$PROFILE" remove tc
  ```

## Conclusion

This is an incomplete list of some of the changes introduced by the
new 1.32 release. For the full list please see the [NEWS
file](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.32.0/NEWS)
and find the source tarball at [the usual location](https://download.gnome.org/sources/NetworkManager/1.32).

And finally, a big Thank You to everybody who contributed.
