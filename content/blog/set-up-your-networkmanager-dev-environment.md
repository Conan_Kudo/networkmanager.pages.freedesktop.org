+++
date = "2021-01-19T12:00:00+01:00"
title = "How to set up your NetworkManager environment [link]"
tags = ["development"]
draft = false
author = "Fernando F. Mancera"
description = "How to contribute to NetworkManager and set up your development environment"
weight = 10
+++

Why should you contribute to NetworkManager?

What do you need to know?

Discover more on [Fernando Mancera's Blog](http://ffmancera.net/networking/networkmanager/2021/01/19/network-manager-dev-env.html).
